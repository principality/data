from os import getenv
from json import dump
from argparse import ArgumentParser
from .cog_data import Cog_Data

parser = ArgumentParser()
parser.add_argument('-g','--group_urls', nargs='+', help='test', required=False, default=[])
parser.add_argument('-p','--project_urls', nargs='+', help='test', required=False, default=[])

if __name__ == '__main__':
    meta = Cog_Data(**vars(parser.parse_args()), auth_token=getenv("READ_TOKEN"))
    with open('cogs.json', 'w+') as file:
        dump(meta.data, file, separators=(',', ':'))