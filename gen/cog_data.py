from typing import List
from requests import get
from toml import loads

class Cog_Data:

    def __init__(self,
        group_urls: List[str] = [],
        project_urls: List[str] = [],
        auth_token: str = None
    ):
        self.data = {'cogs': {}}
        if auth_token:
            self._headers = {"Authorization": "Bearer " + auth_token}
        else:
            self._headers = {}

        for group in group_urls:
            self.parse_group(group)

        for project in project_urls:
            self.parse_project(url=project)

    def parse_group(self, group: str):
        data = self._get_json(group)

        projects = data['projects'] + data['shared_projects']
        for project in projects:
            self.parse_project(data=project)

    def parse_project(self, url: str = None, data: str = None):
        if url:
            data = self._get_json(url)
        if not data:
            return
        project_url = data['_links']['self']
        cogdata = {}

        try:
            metadata = self._get_toml(project_url + '/repository/files/pyproject.toml/raw')
            metadata = {**metadata, **metadata.pop('project')}
        except Exception as error:
            return print(error) # TODO actual error printing

        releases = self._get_json(project_url + '/releases')
        if releases:
            releasedata = releases[0]
            cogdata['latest_version'] = releasedata['tag_name']
            cogdata['download_url'] = [source for source in releasedata['assets']['sources'] if source['format'] == 'zip'][0]['url']
        else:
            cogdata['latest_version'] = metadata['version']
            cogdata['download_url'] = data['namespace']['web_url'] + f"/-/archive/main/{data['path']}-{data['default_branch']}.zip"

        cogdata['name'] = metadata['name']
        cogdata['description'] = metadata['description']
        cogdata['authors'] = [author['name'] for author in metadata['authors']]
        cogdata['stars'] = data['star_count']
        cogdata['topics'] = data['topics']

        if 'avatar_url' in cogdata:
            data['icon_url'] = cogdata['avatar_url']
        
        self.data['cogs'][data['path']] = cogdata

    def _get_json(self, url: str) -> dict:
        return get(url, headers = self._headers).json()
    
    def _get_toml(self, url: str) -> dict:
        return loads( get(url, headers = self._headers).text )